package com.logic.spring_ioc_demo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class IOCApp2 {
	public static void main(String[] args) {
		//1- create the application context (container)
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans-cp.xml"); 
		//here ctx is our IoC container
		//FileSystemXmlApplicationContext is an implementation of ApplicationContext interface.
		//so on line 10 we are "coding to the interface" which is a Java best practice.
		
		//create the bean
		Organization org = (Organization) ctx.getBean("myorg");
		//IoC container injects the bean into our app
		
		//invoke the Organization class' metod via the org bean
		org.saySlogan();
		
		//close the application context
		((ClassPathXmlApplicationContext) ctx).close();
	}
}
